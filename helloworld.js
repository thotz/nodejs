
var express = require('express');
var app = express();

/**	ou now have a pretty capable static file server. Anything you put in the /public folder can now be requested by your browser and displayed. 
HTML, images, almost anything.
ex. http://localhost:3000/iris-header-logo-mobile.png
*/
app.use(express.static(__dirname + '/public'));

app.get('/test', function (req, res) {
  res.send('Hello World! xcxcxc');
});

app.get('/temp', function (req, res) {
  res.send('Hello World! temporary');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

http://blog.modulus.io/absolute-beginners-guide-to-nodejs